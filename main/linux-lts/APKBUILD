# Maintainer: Natanael Copa <ncopa@alpinelinux.org>

_flavor=lts
pkgname=linux-${_flavor}
pkgver=5.4.17
case $pkgver in
	*.*.*)	_kernver=${pkgver%.*};;
	*.*) _kernver=$pkgver;;
esac
pkgrel=0
pkgdesc="Linux lts kernel"
url="http://kernel.org"
depends="mkinitfs"
_depends_dev="perl gmp-dev elfutils-dev bash flex bison"
makedepends="$_depends_dev sed installkernel bc linux-headers linux-firmware-any openssl-dev
	diffutils"
options="!strip"
_config=${config:-config-lts.${CARCH}}
install=
source="https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/linux-$_kernver.tar.xz
	0001-drm-i915-gt-Detect-if-we-miss-WaIdleLiteRestore.patch
	config-lts.aarch64
	config-lts.armv7
	config-lts.x86
	config-lts.x86_64
	config-lts.ppc64le
	config-lts.s390x
	config-lts.mips64

	config-virt.aarch64
	config-virt.armv7
	config-virt.x86
	config-virt.x86_64
	"
subpackages="$pkgname-dev:_dev:$CBUILD_ARCH"
_flavors=
for _i in $source; do
	case $_i in
	config-*.$CARCH)
		_f=${_i%.$CARCH}
		_f=${_f#config-}
		_flavors="$_flavors ${_f}"
		if [ "linux-$_f" != "$pkgname" ]; then
			subpackages="$subpackages linux-${_f}::$CBUILD_ARCH linux-${_f}-dev:_dev:$CBUILD_ARCH"
		fi
		;;
	esac
done

if [ "${pkgver%.0}" = "$pkgver" ]; then
	source="$source
	https://cdn.kernel.org/pub/linux/kernel/v${pkgver%%.*}.x/patch-$pkgver.xz"
fi
arch="all !armhf"
license="GPL-2.0"

_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
mips*) _carch="mips" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

prepare() {
	local _patch_failed=
	cd "$srcdir"/linux-$_kernver
	if [ "$_kernver" != "$pkgver" ]; then
		msg "Applying patch-$pkgver.xz"
		unxz -c < "$srcdir"/patch-$pkgver.xz | patch -p1 -N
	fi

	# first apply patches in specified order
	for i in $source; do
		case $i in
		*.patch)
			msg "Applying $i..."
			if ! patch -s -p1 -N -i "$srcdir"/$i; then
				echo $i >>failed
				_patch_failed=1
			fi
			;;
		esac
	done

	if ! [ -z "$_patch_failed" ]; then
		error "The following patches failed:"
		cat failed
		return 1
	fi

	# remove localversion from patch if any
	rm -f localversion*
	oldconfig
}

oldconfig() {
	for i in $_flavors; do
		local _config=config-$i.${CARCH}
		local _builddir="$srcdir"/build-$i.$CARCH
		mkdir -p "$_builddir"
		echo "-$pkgrel-$i" > "$_builddir"/localversion-alpine \
			|| return 1

		cp "$srcdir"/$_config "$_builddir"/.config
		make -C "$srcdir"/linux-$_kernver \
			O="$_builddir" \
			ARCH="$_carch" HOSTCC="$HOSTCC" \
			listnewconfig oldconfig
	done
}

build() {
	unset LDFLAGS
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"
	for i in $_flavors; do
		cd "$srcdir"/build-$i.$CARCH
		make ARCH="$_carch" CC="${CC:-gcc}" \
			KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-Alpine"
	done
}

_package() {
	local _buildflavor="$1" _outdir="$2"
	local _abi_release=${pkgver}-${pkgrel}-${_buildflavor}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	cd "$srcdir"/build-$_buildflavor.$CARCH
	# modules_install seems to regenerate a defect Modules.symvers on s390x. Work
	# around it by backing it up and restore it after modules_install
	cp Module.symvers Module.symvers.backup

	mkdir -p "$_outdir"/boot "$_outdir"/lib/modules

	local _install
	case "$CARCH" in
		arm*|aarch64) _install="zinstall dtbs_install";;
		*) _install=install;;
	esac

	make -j1 modules_install $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$_outdir" \
		INSTALL_PATH="$_outdir"/boot \
		INSTALL_DTBS_PATH="$_outdir/boot/dtbs-$_flavor"

	cp Module.symvers.backup Module.symvers

	rm -f "$_outdir"/lib/modules/${_abi_release}/build \
		"$_outdir"/lib/modules/${_abi_release}/source
	rm -rf "$_outdir"/lib/firmware

	install -D include/config/kernel.release \
		"$_outdir"/usr/share/kernel/$_buildflavor/kernel.release
}

# main flavor installs in $pkgdir
package() {
	depends="$depends linux-firmware-any"

	_package lts "$pkgdir"
}

# subflavors install in $subpkgdir
virt() {
	_package virt "$subpkgdir"
}

_dev() {
	local _flavor=$(echo $subpkgname | sed -E 's/(^linux-|-dev$)//g')
	local _abi_release=${pkgver}-${pkgrel}-$_flavor
	# copy the only the parts that we really need for build 3rd party
	# kernel modules and install those as /usr/src/linux-headers,
	# simlar to what ubuntu does
	#
	# this way you dont need to install the 300-400 kernel sources to
	# build a tiny kernel module
	#
	pkgdesc="Headers and script for third party modules for $_flavor kernel"
	depends="$_depends_dev"
	local dir="$subpkgdir"/usr/src/linux-headers-${_abi_release}
	export KBUILD_BUILD_TIMESTAMP="$(date -Ru${SOURCE_DATE_EPOCH:+d @$SOURCE_DATE_EPOCH})"

	# first we import config, run prepare to set up for building
	# external modules, and create the scripts
	mkdir -p "$dir"
	cp "$srcdir"/config-$_flavor.${CARCH} "$dir"/.config
	echo "-$pkgrel-$_flavor" > "$dir"/localversion-alpine

	make -j1 -C "$srcdir"/linux-$_kernver O="$dir" ARCH="$_carch" HOSTCC="$HOSTCC" \
		syncconfig prepare modules_prepare scripts

	# remove the stuff that points to real sources. we want 3rd party
	# modules to believe this is the soruces
	rm "$dir"/Makefile "$dir"/source

	# copy the needed stuff from real sources
	#
	# this is taken from ubuntu kernel build script
	# http://kernel.ubuntu.com/git/ubuntu/ubuntu-zesty.git/tree/debian/rules.d/3-binary-indep.mk
	cd "$srcdir"/linux-$_kernver
	find .  -path './include/*' -prune \
		-o -path './scripts/*' -prune -o -type f \
		\( -name 'Makefile*' -o -name 'Kconfig*' -o -name 'Kbuild*' -o \
		   -name '*.sh' -o -name '*.pl' -o -name '*.lds' -o -name 'Platform' \) \
		-print | cpio -pdm "$dir"

	cp -a scripts include "$dir"

	find $(find arch -name include -type d -print) -type f \
		| cpio -pdm "$dir"

	install -Dm644 "$srcdir"/build-$_flavor.$CARCH/Module.symvers \
		"$dir"/Module.symvers

	mkdir -p "$subpkgdir"/lib/modules/${_abi_release}
	ln -sf /usr/src/linux-headers-${_abi_release} \
		"$subpkgdir"/lib/modules/${_abi_release}/build
}

sha512sums="9f60f77e8ab972b9438ac648bed17551c8491d6585a5e85f694b2eaa4c623fbc61eb18419b2656b6795eac5deec0edaa04547fc6723fbda52256bd7f3486898f  linux-5.4.tar.xz
dc0acb47b68c60bede649524fd7429b888df2e1aa16a93f445700d1ffacada8b870a4a75d6aeb33d0e2f2c427efbc80e24a3670c5c539e0fca5b754e7f2a4df3  0001-drm-i915-gt-Detect-if-we-miss-WaIdleLiteRestore.patch
1c155d0fdf8c7e8d33c01f986cef710108407ddc3168604e9202e85090223e76f978eb2617914b39603fe6ec494b639aed39468fad5305a6bccabe5d434e8a5f  config-lts.aarch64
ae0954384f08f80647feaaad22db14cf2fbe6bcc9d6da97133371d5632c291f6a4ddc616b4213f3b8608504729369a86661980ee00285e95a267eede7119a63f  config-lts.armv7
d993eadc676c7f3918198d03eef441d63440cd462e18ce645273314d97073ecb7947304f8a8845ccf1352eb31a9d4466608c648da33e0ca8e910d168e507eaa4  config-lts.x86
69e902555f74d47f85199f69486cba8fd2c4b27fa5a364fa1bca01da67dff463b0c441abd7c63b26822d4a5d31d8e0bdf3aa7369886d3d12c1483375ab83c38c  config-lts.x86_64
1afe13e8fc474b5503cd117030bce06403f51c0e1962bcc56c5bd783343f509bec47a02ebcda895640d582ee735e320448c8d0011885b537ffe72b7bb0071e96  config-lts.ppc64le
80730cf979fac9a09946202b8db2d1db7ef2e0082f5893ca5da502560bca5067e91d6615dca36e19203fa6fa0ba44c2f9bd4e59ff930f4d5967e24df7347e0b4  config-lts.s390x
03f523471d01603beaacbf9c82df56263e488bbbeb445ffc602888cd3422387133cd13598d62109fb816e47d2542b55a0ad75326d0a9344026dbc264e5796989  config-lts.mips64
b2789e33e11f33cf464a7a7c8cf9aad0ad8e66615178268502a54abad654672e1bb0f5cfe8c18af4cf58696f41e40376920e14cd736ec10e2c279c380c924fec  config-virt.aarch64
3640753d114a4337d947b9864b7c27ffe1ce5b174627e5e009050318f0f7e8e8814ae2792e46a10f9de6c03745afd6f38bd9a7ec76466007918063896ec3c869  config-virt.armv7
b7217ba4f92a3a6c6abbcc873527b99153281d56772f4737888b79f1d5741838ff6dd1ec11455c77f852b2ae7085baabdf27e1f83b6e0d0c2be535f586844fa5  config-virt.x86
a8707144671e653eb5d7ef6be087521bc14e20790d42636cc5a9ffdcc9599f2a5503a49644387995a57f25a3ab980506f64ced1981ed04760075c05ccbc1532f  config-virt.x86_64
751b3d46f3d3b6bb994a793bb74ca12abe1f9b6d4e45bff6eaab4e3839b8f37a1e4d4dec28ba68648a5c25098ac027995eaebbab13bdebb1f490da379bbaa079  patch-5.4.17.xz"
